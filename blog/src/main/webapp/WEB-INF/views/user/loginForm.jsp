<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../layout/header.jsp"%>

<div class="container">
	<form action="/auth/loginProc" method="post">
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" class="form-control" placeholder="Enter username" id="username" name="username">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" placeholder="Enter password" id="password" name="password">
		</div>
		<button id="btn-login" type="submit" class="btn btn-primary">로그인</button>
		<a href="https://kauth.kakao.com/oauth/authorize?client_id=5768b3b8e808a7538f93d46fdae303a3&redirect_uri=http://localhost:7999/auth/kakao/callback&response_type=code"><img alt="" height="38px;" src="/image/kakao_login_button.png"></a>
	</form>

</div>

<%@ include file="../layout/footer.jsp"%>

