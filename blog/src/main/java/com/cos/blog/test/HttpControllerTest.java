package com.cos.blog.test;

import javax.servlet.jsp.tagext.Tag;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.experimental.PackagePrivate;

// 사용자가 요청 -> 응답(html 파일)
// @controller

// 사용자가 요청 -> 응답(Data)
@RestController
public class HttpControllerTest {
	
	private static final String Tag = "HttpControllerTest : ";

	@GetMapping("http/lombok")
	public String lombokTest() {
		//Member m = new Member(1, "asd", "sdf", "dfg");
		Member m = Member.builder().username("asdasd").password("asdasd").email("asdasd").build();
		
		System.out.println(Tag + "getter : " + m.getUsername());
		m.setUsername("dddddddddddddddddddddddddd");
		System.out.println(Tag + "getter : " + m.getUsername());

		return "lombok test 완료";
	}
	
	// http://localhost:8000/http/get (select)
	@GetMapping("/http/get")
	public String getTest(Member m) {
		return "get 요청" + m.getId() + m.getUsername();
	}

	// http://localhost:8000/http/post (insert)
	@PostMapping("/http/post")
	public String postTest() {

		return "post 요청";
	}

	// http://localhost:8000/http/put (update)
	@PutMapping("/http/put")
	public String putTest() {

		return "put 요청";
	}

	// http://localhost:8000/http/delete (delete)
	@DeleteMapping("/http/delete")
	public String deleteTest() {

		return "delete 요청";
	}

}
