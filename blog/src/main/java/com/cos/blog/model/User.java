package com.cos.blog.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity // user 클래스가 mysql에 테이블이 생성이 된다.
// @DynamicInsert null 테이블 제외해주고 쿼리실행
public class User {

	@Id // primart key
	@GeneratedValue(strategy = GenerationType.IDENTITY) // 프로젝트에서 연결된 db의 넘버링 전략을 따라간다.
	private int id; // 시퀀스, auto_increment

	@Column(nullable = false,length = 100, unique = true)
	private String username; // 아이디

	@Column(nullable = false,length = 100) // 암호화 
	private String password;

	@Column(nullable = false,length = 50)
	private String email;

	//@ColumnDefault("'user'")
	@Enumerated(EnumType.STRING)
	private RoleType role; // Enum을 쓰는게 좋다. // ADMIN, USER
	
	
	private String oauth; // 카카오 로그인
	
	
	@CreationTimestamp // 시간이 자동입력
	private Timestamp createDate;
	
}
